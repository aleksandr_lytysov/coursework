#pragma once
class DataLoader
{
public:
	struct Employee;
	DataLoader();
	static Employee* Load();
	~DataLoader();
};

